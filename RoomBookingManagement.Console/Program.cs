﻿using System;

namespace RoomBookingManagement.Console
{
    using RoomBookingManagement.Services;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    class Program
    {
        static async Task Main()
        {
            if (!TryGetFilePath(out string filePath))
            {
                PrintError("A file path is expected. Try adding 'file=[file_path]' to the command line.");
            }
            else
            {
                Console.WriteLine($"Reading bookings from {filePath}");

                // Load bookings
                IBookingProvider bookingProvider = new CsvFileBookingProvider(filePath);

                var bookings = await bookingProvider.GetBookings();
                Console.WriteLine($"{bookings.Count} bookings found.");

                // Print any conflicts
                var conflicts = Bookings.GetConflicts(bookings).ToList();
                if (conflicts.Count > 0)
                {
                    PrintError($"{conflicts.Count} conflicts detected.");
                    foreach (var conflict in conflicts)
                    {
                        var message = $"{conflict.booking.Name} conflicts with {conflict.conflict.Name}";

                        PrintError(message);
                    }
                }
            }
        }

        private static bool TryGetFilePath(out string filePath)
        {
            var match = System.Text.RegularExpressions.Regex.Match(Environment.CommandLine, "file=(?<filePath>(\".+ \")|([^\\s]+))");
            filePath = match.Groups["filePath"].Value;
            return match.Success;
        }

        static void PrintError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
