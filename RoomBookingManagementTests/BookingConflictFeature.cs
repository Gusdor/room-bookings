using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomBookingManagement;
using RoomBookingManagement.Domain;
using FluentAssertions;

namespace RoomBookingManagementTests
{
    [TestClass]
    public class BookingConflictFeature
    {
        readonly Context context = new Context();

        [TestMethod]
        public void ConflictsAreDetected()
        {
            GivenBookings(
                (name: "B1", startHour: 1, endHour: 3),
                (name: "B2", startHour: 1, endHour: 2),
                (name: "B3", startHour: 3, endHour: 5),
                (name: "B4", startHour: 4, endHour: 6),
                (name: "B5", startHour: 7, endHour: 10),
                (name: "B6", startHour: 8, endHour: 9));

            WhenBookingsAreAssessedForConflicts();

            ThenConflictsAreDetectedBetweenBookings(
                ("B1", "B2"),
                ("B3", "B4"),
                ("B5", "B6"));
        }

        [TestMethod]
        public void NoConflictsArePresent()
        {
            GivenNoConflicts();

            WhenBookingsAreAssessedForConflicts();

            ThenNoConflictsAreDetected();
        }

        private void ThenNoConflictsAreDetected()
        {
            context.Conflicts.Should().BeEmpty();
        }

        private void GivenNoConflicts()
        {
            var date = new DateTime();
            context.Bookings.Add(new Booking("Booking 1", "Big Room", date, date.AddHours(1)));
            context.Bookings.Add(new Booking("Booking 2", "Small Room", date, date.AddHours(1)));
        }

        private void WhenBookingsAreAssessedForConflicts()
        {
            var bookings = context.Bookings;
            var conflicts = Bookings.GetConflicts(bookings);
            context.Conflicts.AddRange(conflicts);
        }

        private void ThenConflictsAreDetectedBetweenBookings(params (string booking, string conflict)[] conflicts)
        {
            // Flatten the context out to a collection that matches the specification.
            var fromContext =
                from p in context.Conflicts
                select (p.booking.Name, p.conflict.Name);

            conflicts.Should().BeEquivalentTo(fromContext);
        }

        private void GivenBookings(params (string name, int startHour, int endHour)[] bookings)
        {
            DateTime day = new DateTime();
            string room = "Test Room";
            var mappings = bookings.Select(
                p => new Booking(p.name, room, day.AddHours(p.startHour), day.AddHours(p.endHour)));

            context.Bookings.AddRange(mappings);
        }

        private class Context
        {
            public List<Booking> Bookings { get; } = new List<Booking>();
            public List<(Booking booking, Booking conflict)> Conflicts { get; } = new List<(Booking, Booking)>();
        }
    }

}
