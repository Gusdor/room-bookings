﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RoomBookingManagement.Domain;

namespace RoomBookingManagement.Services
{
    /// <summary>
    /// Provides room bookings.
    /// </summary>
    public interface IBookingProvider
    {
        /// <summary>
        /// Gets a list of bookings.
        /// </summary>
        /// <returns>A task that resolves with a list of <see cref="Booking"/>.</returns>
        Task<List<Booking>> GetBookings();
    }
}