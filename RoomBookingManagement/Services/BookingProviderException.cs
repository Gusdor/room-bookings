﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomBookingManagement.Services
{

    [Serializable]
    public class BookingProviderException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BookingProviderException"/> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public BookingProviderException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BookingProviderException"/> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception that provides more detail.</param>
        public BookingProviderException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
