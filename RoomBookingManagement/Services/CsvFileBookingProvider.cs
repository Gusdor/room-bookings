﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using RoomBookingManagement.Domain;

namespace RoomBookingManagement.Services
{
    /// <summary>
    /// Retrives bookings from a CSV file.
    /// </summary>
    public class CsvFileBookingProvider : IBookingProvider
    {
        private readonly string filePath;

        /// <summary>
        /// Initializes a new instance of the <see cref="CsvFileBookingProvider"/> class.
        /// </summary>
        /// <param name="filePath">The file path to read the bookings from.</param>
        public CsvFileBookingProvider(string filePath)
        {
            this.filePath = filePath;
        }

        /// <inheritdoc/>
        public Task<List<Booking>> GetBookings()
        {
            // Csv helper does not provide async wrappers, but this is an IO bound operation
            // and the a Task is required by the interface.
            return Task.Run(() =>
            {
                try
                {
                    using (var reader = new System.IO.StreamReader(this.filePath))
                    using (var csv = new CsvReader(reader))
                    {
                        var records = csv.GetRecords<BookingRecord>();

                        var result = records.Select(p => new Booking(p.Name, p.Room, p.Start, p.End));

                        return result.ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw new BookingProviderException("An exception occurred while reading bookings", ex);
                }
            });
        }

        private class BookingRecord
        {
            public string Name { get; set; }

            public string Room { get; set; }

            public DateTime Start { get; set; }

            public DateTime End { get; set; }
        }
    }
}
