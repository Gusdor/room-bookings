﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomBookingManagement.Domain
{
    /// <summary>
    /// Represents a range between two values.
    /// </summary>
    /// <typeparam name="T">The type of value that defines the range boundaries.</typeparam>
    internal class Range<T>
        where T : IComparable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Range{T}"/> class.
        /// </summary>
        /// <param name="start">The start of the range.</param>
        /// <param name="end">The end of the range.</param>
        public Range(T start, T end)
        {
            this.Start = start;
            this.End = end;
        }

        /// <summary>
        /// Gets the start of the range.
        /// </summary>
        public T Start { get; }

        /// <summary>
        /// Gets the end of the range.
        /// </summary>
        public T End { get; }

        /// <summary>
        /// Computes if two ranges overlaps.
        /// </summary>
        /// <param name="comparer">The range to compare.</param>
        /// <returns><c>true</c> if the ranges overlap.</returns>
        public bool Overlaps(Range<T> comparer)
        {
            return this.Start.CompareTo(comparer.End) < 0
                && comparer.Start.CompareTo(this.End) < 0;
        }
    }
}
