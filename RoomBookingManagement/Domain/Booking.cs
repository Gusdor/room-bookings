﻿using System;

namespace RoomBookingManagement.Domain
{
    /// <summary>
    /// A basic room booking with no additional information.
    /// </summary>
    public class Booking
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Booking"/> class.
        /// </summary>
        /// <param name="name">The name of the booking.</param>
        /// <param name="room">The room that this booking will occupy.</param>
        /// <param name="start">The stat time for the booking.</param>
        /// <param name="end">The end time for the booking.</param>
        public Booking(string name, string room, DateTime start, DateTime end)
        {
            this.Range = new Range<DateTime>(start, end);
            this.Name = name;
            this.Room = room;
        }

        /// <summary>
        /// Gets the name of the booking.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the room that is occupied by the booking.
        /// </summary>
        public string Room { get; }

        /// <summary>
        /// Gets time range that defines this bookings start and duration.
        /// </summary>
        internal Range<DateTime> Range { get; }
    }
}
