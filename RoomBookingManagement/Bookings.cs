﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoomBookingManagement.Domain;

namespace RoomBookingManagement
{
    /// <summary>
    /// Provides utilities for working with bookings.
    /// </summary>
    public static class Bookings
    {
        /// <summary>
        /// Gets the conflicts, if any, that occur in a sequence of bookings.
        /// </summary>
        /// <param name="bookings">The bookings to check for conflicts.</param>
        /// <returns>A sequence of <see cref="Booking"/> instances that are conflicted.</returns>
        public static IEnumerable<(Booking booking, Booking conflict)> GetConflicts(List<Booking> bookings)
        {
            var results = new List<Booking[]>();

            foreach (var booking in bookings)
            {
                var conflicts = from b in bookings
                                where b != booking
                                && booking.Room == b.Room
                                && booking.Range.Overlaps(b.Range)
                                select b;

                foreach (var conflict in conflicts)
                {
                    var sorted = new[] { booking, conflict }.OrderBy(p => p.Name).ToArray();

                    // Only add conflicting pairs that have not already occurred.
                    if (!results.Any(p => p.SequenceEqual(sorted)))
                    {
                        results.Add(sorted);
                    }
                }
            }

            return results.Select(p => (p[0], p[1]));
        }
    }
}
